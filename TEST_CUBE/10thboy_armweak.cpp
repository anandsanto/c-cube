//CodeChef submission 6399672 (C++ 4.3.2) plaintext list. Status: AC, problem CCUBE04, contest CCBE2015. By japoorv (japoorv), 2015-03-05 23:30:19.
#include <iostream>
#include <cstdio>
#include <cmath>
#include <cstring>
#define getnum(c) c-'0'
using namespace std;
long long  int arr[10][7]={0};
long long int ini[]={0,0,10,100,1000,10000,100000,1000000,10000000,100000000};
long long int dest[]={0,9,99,999,9999,99999,999999,9999999,99999999,999999999};
long long calcu(int a,int b)
{
    long long ab(1);
    for (int c(1);c<=b;c++)ab*=a;
    return ab;
}
void build()
{
    arr[0][0]=1;
for (int c(1);c<=9;c++)
{
    for (int c1(0);c1<=6;c1++)
    {
        for (int c2(0);c2<=9;c2++)
        {
            arr[c][c1]+=arr[c-1][(c1+49LL-(49LL+calcu(c2,c))%7)%7];
        }
    }

}


return;
}

int main ()
{

build();
int n;
scanf("%d",&n);

for (;n>0;n--)
{
 int a;
 scanf("%d",&a);
 long long total(0);
 for (int c(1);c<=a;c++)
 {
     int total1(0);
     for (int h(c),j(1);h!=0;h=(h-h%10)/10,j++){long long tempo=calcu(h%10,j)%7;total1+=tempo;}
     if (total1%7==0)total++;
 }
 cout << total << endl;
}
return 0;
}
