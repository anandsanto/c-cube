//CodeChef submission 6399569 (C++14) plaintext list. Status: AC, problem CCUBE05, contest CCBE2015. By danieljames (danieljames), 2015-03-05 23:19:12.
#include <iostream>
#include <string.h>
#include <algorithm>

using namespace std;

void cl(char *s,int l) {
    for(int i=0;i<l;i++) *(s+i)='0';
}

int abc(char *s,char *a) {
    int l=strlen(a),y=strlen(s);
    for(int i=0;i<y-l;i++) {
        if(strncmp(s+i,a,l)==0) {
            cl(s+i,l); return 1;
        }
    }
    return 0;
}

int main()
{
    char s[1001];
    char st[][11]={"sun","sirius","canopus","arcturus","vega","capella","rigel","procyon","achernar","betelgeuse","hadar","acrux","altair","aldebaran","antares","spica","pollux","fomalhaut","becrux","deneb","regulus","adhara","castor","gacrux","shaula"};
    cin>>s;
    int i,n;
    for(i=0;i<25;i++) {
        n=0;
        reverse(st[i],st[i]+strlen(st[i]));
        while(abc(s,st[i])) {
            n++;
        }
        reverse(st[i],st[i]+strlen(st[i]));
        while(abc(s,st[i])) n++;
        cout<<n<<endl;
    }

   return 0;
}
