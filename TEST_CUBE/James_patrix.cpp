//CodeChef submission 6400151 (C++14) plaintext list. Status: AC, problem CCUBE03, contest CCBE2015. By danieljames (danieljames), 2015-03-06 00:40:43.
#include <iostream>
#include <stdlib.h>

using namespace std;

int A[102][102],m,n;

void swp(int &a,int &b) {
    int t;
    t=a;
    a=b;
    b=t;
}

int hcf(int x,int y) {
    while (x!=y) {
        if(x>y) x-=y;
        else y-=x;
    }
    return x;
}

int confirmer(int a,int b,int i,int j) {
    int p;
    if(a==i) {
        if(j>b) swp(j,b);
        for(p=j+1;p<n;p++) if(A[a][p]) return 0;
    }
    if(b==j) {
        if(i>a) swp(i,a);
        for(p=i+1;p<a;p++) if(A[p][b]) return 0;
    }
    int x,y;
    x=abs(a-i);
    y=abs(b-j);
    if(x==0||y==0) return 1;
    if(hcf(x,y)==1) return 1;
    else return 0;
}

int cnt(int a,int b,int p) {
    int s=0,i,j;
    if(p==1) return 1;
    A[a][b]=0;
    for(i=1;i<=m;i++) {
        for(j=1;j<=n;j++) {
            if(A[i][j]==1) {
               if(confirmer(a,b,i,j)) s+=cnt(i,j,p-1);
            }
        }
    }
    A[a][b]=1;
    return s;
}

int main()
{
    int i,j,t,p,s;
    cin>>t;
    while(t--) {
        s=0;
        cin>>m>>n>>p;
        for(i=0;i<=m+1;i++) A[i][0]=A[i][n+1]=0;
        for(i=0;i<=n+1;i++) A[0][i]=A[m+1][i]=0;
        for(i=1;i<=m;i++) {
            for(j=1;j<=n;j++)
                A[i][j]=1;
        }
        for(i=1;i<=m;i++) {
            for(j=1;j<=n;j++) {
                s+=cnt(i,j,p);
            }
        }
        cout<<s<<endl;
    }
   return 0;
}
