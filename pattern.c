/*
 ================================================================================================================
 Name        :  Pattern_Android.c
 Author      :  Anandsanto, Gowtham
 Question    :
 ================================================================================================================
 */
#include<stdio.h>
#define MAX_PATTERN_SIZE_ROW 100
#define MAX_PATTERN_SIZE_COL 100
int engaged_dots[MAX_PATTERN_SIZE_ROW][MAX_PATTERN_SIZE_COL];
int pattern_size_row, pattern_size_col;
int counter,counter1;
int Max_Counter;
long long result;

int modu(int a){
    if(a>=0) return a;
    return (-a);
}
int findmax(int a, int b){
			if(a>b) return a;
			return b;
		}
int findmin(int a, int b){
    if(a<b) return a;
    return b;
}
void connect_a_dot(int cur_row, int cur_col, int temp_read[][MAX_PATTERN_SIZE_COL]){

    int i,j,k,l,m,n,flag1,p,q;//,photo_store_i,photo_store_j;
    int temp_read_1[MAX_PATTERN_SIZE_ROW][MAX_PATTERN_SIZE_COL];

    // Take a photograph
    for(i=0;i<pattern_size_row;i++){
        for(j=0;j<pattern_size_col;j++){
            temp_read_1[i][j]=engaged_dots[i][j];
            //printf("%d\t",temp_read_1[i][j]);
        }
        //printf("\n");
    }
    //printf("pasd_row : %d \n pasd_col %d ",cur_row,cur_col);
    //printf("\n");
    for(i=0;i<pattern_size_row;i++){
        for(j=0;j<pattern_size_col;j++){

            /*for(k=0;k<pattern_size_row;k++){
                    for(l=0;l<pattern_size_col;l++){
                        //temp_read_1[i][j]=engaged_dots[i][j];
                        printf("%d\t",engaged_dots[k][l]);
                    }
                    printf("\n");
                }
           // printf("\n");*/
            if(engaged_dots[i][j] == 0) {

                flag1 = 1;
                 //printf("\n");
                 //printf("pasd row : %d \t pasd col: %d\ncurr_row : %d \t cur_col %d \n",cur_row,cur_col,i,j);

            if(!(cur_col==0 && cur_row==0 && engaged_dots[0][0]==0))
                {
                 //printf("\n hello \n");
                 if((modu(i - cur_row) > 1 && j==cur_col)){
                    //printf("pasd row : %d \n pasd col: %d\n i = %d \n j = %d\n",cur_row,cur_col,i,j);
                    for(m=(findmin(cur_row,i)+1);m<findmax(i,cur_row);m++){
                        if(engaged_dots[m][j]==0){
                           // printf("I hav entered and I've seen a zero at (%d,%d)\n",m,j);
                            flag1 = 0;
                            break;
                        }
                    }
                }

                if((modu(cur_col - j) > 1 && i==cur_row)){
                        for(m=(findmin(cur_col,j)+1);m<findmax(cur_col,j);m++){
                            if(engaged_dots[cur_row][m]==0){
                                flag1 = 0;
                                break;
                            }
                        }
                }

                if((modu(i-cur_row) == modu(j-cur_col)) && modu(i-cur_row) > 1){
                    //printf("hello");
                    for(m=1;m<(modu(j-cur_col));m++){
                        if(cur_col>j && cur_row>i){
                            if(engaged_dots[i + m][j + m] == 0){
                                flag1 = 0;
                                break;
                            }
                        }
                        if(cur_col<j && cur_row>i){
                            if(engaged_dots[i + m][j - m] == 0){
                                flag1 = 0;
                                break;
                            }
                        }
                        if(cur_col<j && cur_row<i){
                            if(engaged_dots[i - m][j - m] == 0){
                                flag1 = 0;
                                break;
                            }
                        }
                        if(cur_col>j && cur_row<i){
                            if(engaged_dots[i - m][j + m] == 0){
                                flag1 = 0;
                                break;
                            }
                        }
                    }
                }
                }
                if(flag1==0) {
                   // printf("\n hello exit \n");
                    continue;

                }
                //flag=1;
                // make it one
                engaged_dots[i][j] = 1;
                //Check if all the dots are connected
                counter1 = 0;
                for(k=0;k<pattern_size_row;k++)
                    for(l=0;l<pattern_size_col;l++){
                        if(engaged_dots[k][l]==1) counter1++;
                    }
                // If all the dots are connected, increment result return to the place of call
                if(counter1==Max_Counter){
                    result+=1;
                    //printf("\n %d is result\n",result);
                    /*for(k=0;k<pattern_size_row;k++)
                        for(l=0;l<pattern_size_col;l++)
                            engaged_dots[k][l]=temp_read[k][l];
                    //return;*/
                    goto HERE;
                }
                connect_a_dot(i,j,temp_read_1);
                continue;
                HERE: m=1;
                engaged_dots[i][j] = 0;
            }
        }
    }
    //Load the photo back :P
    for(i=0;i<pattern_size_row;i++)
        for(j=0;j<pattern_size_col;j++)
            engaged_dots[i][j]=temp_read[i][j];
}
int main(){
    int test,k;
    int col_size[1000],row_size[1000],pat_size[1000];
    long long ans[1000];
    scanf("%d",&test);
    for(k=0;k<test;k++){
        scanf("%d %d %d",&row_size[k],&col_size[k],&pat_size[k]);
    }
    for(k=0;k<test;k++){
    pattern_size_col = col_size[k];
    pattern_size_row = row_size[k];
    Max_Counter = pat_size[k];
    result = 0;
    int i,j,temp_read[MAX_PATTERN_SIZE_ROW][MAX_PATTERN_SIZE_COL];
    /*scanf("%d",&Max_Counter);
    scanf("%d",&pattern_size_row);
    scanf("%d",&pattern_size_col);*/
    for(i=0;i<pattern_size_row;i++)
        for(j=0;j<pattern_size_col;j++)
            engaged_dots[i][j]=0;
    for(i=0;i<MAX_PATTERN_SIZE_ROW;i++)
        for(j=0;j<MAX_PATTERN_SIZE_COL;j++)
            temp_read[i][j]=0;
    connect_a_dot(0,0,temp_read);
    //printf("%lld\n",result);
    ans[k] = result;
    }
    for(k=0;k<test;k++){
        printf("%lld\n",ans[k]);
    }
    return 0;
}
