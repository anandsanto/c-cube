/* Once a letter is selected for forming a word it CANNOT be reused to form any other word.
*Stars will have to be listed in preference order, eg SIRIUSUN it will detect either
*sirius or sun based on preference
The program I've written takes the star array as default preference i.e sun comes before
sirius in the array and hence sun will be preferred over sirius.
*/

#include<stdio.h>
#include<string.h>
#define star_no 25 // No of elements in star array goes here
int main(){
    int test,index,l;
    scanf("%d",&test);
    char ipstr[1001]; // Max i/p string size
    int ipd[1000];
    char dic[][12] = {"sun","sirius","canopus","arcturus","vega","capella","rigel","procyon","achernar",
 "betelgeuse","hadar","acrux","altair","aldebaran","antares","spica","pollux","fomalhaut","becrux","deneb",
 "regulus","adhara","castor","gacrux","shaula"};//star names go here
    int diccnt[star_no];
    for(index=0;index<1000;index++) ipd[index] = 0;
    for(index=0;index<star_no;index++) diccnt[index] = 0;
    int i,j,flag;
    int k;
    for(index=0;index<test;index++){
        scanf("%s",&ipstr);
        for(i=0;i<star_no;i++){
            for(j=0;j<strlen(ipstr);j++){
                flag = 0;
                if(dic[i][0] == ipstr[j] && ipd[j] != 1){
                    for(k=1;k<strlen(dic[i]);k++){
                        if(dic[i][k] == ipstr[j+k] && ipd[j+k] != 1) continue;
                        flag = 1;
                        break;
                    }
                    if(flag==0){
                        for(k=j;k<(strlen(dic[i])+j);k++) ipd[k] = 1;
                        //for(l=0;l<strlen(ipstr);l++) printf("%d\t",ipd[l]);
                        printf("\n");
                        diccnt[i]++;
                    }
                }
            }
            for(j=(strlen(ipstr)-1);j>=0;j--){
                flag = 0;
                if(dic[i][0] == ipstr[j] && ipd[j] != 1){
                    for(k=1;k<strlen(dic[i]);k++){
                        if(dic[i][k] == ipstr[j-k] && ipd[j-k] == 0) continue;
                        flag = 1;
                        break;
                    }
                    if(flag==0){
                        for(k=j;k>(j-strlen(dic[i]));k--) ipd[k] = 1;
                        //for(l=0;l<strlen(ipstr);l++) printf("%d\t",ipd[l]);
                        printf("\n");
                        diccnt[i]++;
                    }
                }
            }
        }
    }
    for(k=0;k<star_no;k++) printf("%d\n",diccnt[k]);// = 0;
    return 0;
}
